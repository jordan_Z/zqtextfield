//
//  AppDelegate.h
//  ZQTextField
//
//  Created by 张群 on 2018/11/21.
//  Copyright © 2018 ZQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

